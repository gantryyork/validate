from enum import Enum, auto


class Integer(Enum):
    Gen = auto()
    Min = auto()
    Max = auto()
    MinMax = auto()
    InSet = auto()


def general(value):
    return True


def minmax(value, **kwargs):
    if "min" not in kwargs:
        return False

    if "max" not in kwargs:
        return False

    if kwargs.get("min") <= value and value <= kwargs.get("max"):
        return True

    return False
