import validate.string
import validate.integer
import validate.float


ALL_TYPES = (
    list(validate.string.String)
    + list(validate.integer.Integer)
    + list(validate.float.Float)
)


def is_valid(value, type=validate.string.String.Gen, **kwargs):

    # Check all valid types
    if type not in ALL_TYPES:
        # It actually trows an AttributeError before it throws a NameError
        raise NameError
        return False

    # Compare value to correct type
    if type in list(validate.string.String):
        if not isinstance(value, str):
            raise TypeError
    elif type in list(validate.integer.Integer):
        if not isinstance(value, int):
            raise TypeError
    elif type in list(validate.float.Float):
        if not isinstance(value, float):
            raise TypeError
    else:
        # impossible to reach this else
        raise TypeError

    # Dispatch to validation function
    if type == validate.string.String.Gen:
        return validate.string.general(value)
    elif type == validate.string.String.Hex:
        return validate.string.hex(value)
    elif type == validate.string.String.IPv4:
        return validate.string.ipv4(value)
    elif type == validate.string.String.RomanNumeral:
        return validate.string.roman_numeral(value)
    elif type == validate.integer.Integer.Gen:
        return validate.integer.general(value)
    elif type == validate.integer.Integer.MinMax:
        return validate.integer.minmax(value, **kwargs)
    elif type == validate.float.Float.Gen:
        return validate.float.general(value)
