from enum import Enum, auto


class Float(Enum):
    Gen = auto()
    Min = auto()
    Max = auto()
    MinMax = auto()
    Lat = auto()
    Lon = auto()


def general(value):
    return True
