# from validate.data import is_valid, String, Integer, Float
from enum import Enum, auto
import re
import validate.data as check
import validate.integer


class String(Enum):
    Gen = auto()
    Hex = auto()
    IPv4 = auto()
    RomanNumeral = auto()
    MaxLen = auto()


def general(value):
    return True


def hex(value):
    for character in value:
        if character.upper() not in [
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
        ]:
            return False
    return True


def ipv4(value):
    octets = value.split(".")
    if len(octets) != 4:
        return False
    for octet in octets:
        if not check.is_valid(int(octet), validate.integer.Integer.MinMax, min=0, max=255):
            return False
    return True


def roman_numeral(value):

    return bool(re.search(r"(?<![MDCLXVI])(?=[MDCLXVI])M*(CM|CD|D?C*)(XC|XL|XM|L?X*)(IX|IV|I+M|V?I*)$", value))
