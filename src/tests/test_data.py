import pytest
from validate.data import is_valid
from validate.string import String
from validate.integer import Integer
from validate.float import Float


class TestNegatives:

    def test_invalid_typename(self):
        with pytest.raises(AttributeError):
            is_valid("Anything", String.NonExistant)

    def test_invalid_type(self):
        with pytest.raises(TypeError):
            is_valid("String", Integer.Gen)


class TestBasicTypes:

    def test_default(self):
        assert is_valid("XYZ")

    def test_str_valid(self):
        assert is_valid("ABC", String.Gen)
        assert is_valid("ABC", String.Gen, extra='abc')

    def test_int_valid(self):
        assert is_valid(10, Integer.Gen)
        assert is_valid(10, Integer.Gen, extra=123)

    def test_float_valid(self):
        assert is_valid(10.0, Float.Gen)
        assert is_valid(10.0, Float.Gen, extra=999)


class TestStrVariations:

    def test_hex_valid(self):
        assert is_valid("0123456789abcdef", String.Hex)
        assert is_valid("089AABbffFF", String.Hex)
        assert is_valid("0123456789abcdef", String.Hex, extra=999)

    def test_hex_invalid(self):
        assert not is_valid("0189afgh", String.Hex)

    def test_ipv4_valid(self):
        assert is_valid("127.0.0.1", String.IPv4)
        assert is_valid("255.255.255.255", String.IPv4)
        assert is_valid("0.0.0.0", String.IPv4)
        assert is_valid("10.10.10.10", String.IPv4)

    def test_ipv4_invalid(self):
        assert not is_valid("1.1.1.256", String.IPv4)
        assert not is_valid("55.66.77.88.99", String.IPv4)

    def test_roman_valid(self):
        assert is_valid("XVI", String.RomanNumeral)
        assert is_valid("IV", String.RomanNumeral)
        assert is_valid("XC", String.RomanNumeral)
        assert is_valid("CM", String.RomanNumeral)
        assert is_valid("XV", String.RomanNumeral)
        assert is_valid("MMMDCCXXIV", String.RomanNumeral)  # 3724
        assert is_valid("MMMM", String.RomanNumeral)    # 4000
        assert is_valid("MMMMI", String.RomanNumeral)   # 4001
        assert is_valid("IIII", String.RomanNumeral)    # 4
        assert is_valid("IIM", String.RomanNumeral)     # 998
        assert is_valid("IV", String.RomanNumeral)  # 4
        assert is_valid("CMXC", String.RomanNumeral)    # 990
        assert is_valid("XM", String.RomanNumeral)  # 990
        assert is_valid("MMMMMMMMMM", String.RomanNumeral)  # 10000

    def test_roman_invalid(self):
        assert not is_valid("", String.RomanNumeral)
        assert not is_valid("xv", String.RomanNumeral)
        assert not is_valid("IVXLCDMS", String.RomanNumeral)
        assert not is_valid("LL", String.RomanNumeral)
        assert not is_valid("DD", String.RomanNumeral)
        assert not is_valid("VV", String.RomanNumeral)
        assert not is_valid("DM", String.RomanNumeral)
        assert not is_valid("LC", String.RomanNumeral)
        assert not is_valid("11", String.RomanNumeral)


class TestIntVariations:

    def test_int_valid(self):
        assert is_valid(10, Integer.Gen)
        assert is_valid(10, Integer.Gen, extra=999)

    def test_int_in_range(self):
        assert is_valid(2, Integer.MinMax, min=-5, max=5)
        assert is_valid(0, Integer.MinMax, min=-5, max=5)
        assert is_valid(-5, Integer.MinMax, min=-5, max=5)
        assert is_valid(5, Integer.MinMax, min=-5, max=5)
        assert is_valid(5, Integer.MinMax, min=-5, max=5, extra=999)

    def test_int_in_range_missing_param(self):
        assert not is_valid(5, Integer.MinMax, min=0)
        assert not is_valid(5, Integer.MinMax, max=5)

    def test_int_out_of_range(self):
        assert not is_valid(-6, Integer.MinMax, min=-5, max=5)
        assert not is_valid(6, Integer.MinMax, min=-5, max=5)


class TestFloatVariations:

    def test_valid_float(self):
        assert is_valid(3.14, Float.Gen)
